package com.example.edwin.ecuacionEstado;

import android.content.Intent;
import android.os.Build;
import android.os.Parcelable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import static com.example.edwin.ecuacionEstado.R.id.parent;

public class Calculo extends AppCompatActivity {

    // Declaración de variables
    IdealGasInterface operacion;
    double unidad_1 = 0.0, unidad_2 = 0.0;
    TextView textoResultado;
    EditText campo1;
    EditText campo2;
    EditText campo3;
    HashMap<String, Double> valoresCampos = new HashMap<>();
    String[] arrayUnidades1, arrayUnidades2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperatura);

        // Se recibe la actividad anterior de la clase EcuacionGasIdeal
        Intent intent = getIntent();
        operacion = intent.getParcelableExtra(EcuacionGasIdeal.VARIABLE);

        //Inicialización de variables
        TextView tituloVariable = (TextView) findViewById(R.id.totalTextView);
        textoResultado = (TextView) findViewById(R.id.Resultado);
        campo1 = (EditText) findViewById(R.id.Campo1);
        campo2 = (EditText) findViewById(R.id.Campo2);
        campo3 = (EditText) findViewById(R.id.Campo3);
        Spinner spUnidades1 = (Spinner) findViewById(R.id.Unidades1);
        Spinner spUnidades2 = (Spinner) findViewById(R.id.Unidades2);


        // Modificar todos los campos con datos iniciales
        tituloVariable.setText(operacion.getName());
        campo1.setHint(operacion.getInputs()[0]);
        campo2.setHint(operacion.getInputs()[1]);
        campo3.setHint(operacion.getInputs()[2]);

        // Llenar listas de unidades
        arrayUnidades1 = new String[] { operacion.getUnits()[0], operacion.getUnits()[1],
        operacion.getUnits()[2], operacion.getUnits()[3]};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, arrayUnidades1);
        spUnidades1.setAdapter(adapter1);
        spUnidades1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    switch (position) {
                        case 0:
                            break;
                        case 1:
                            unidad_1 = Double.parseDouble(campo1.getText().toString());
                            break;
                        case 2:
                            unidad_1 = (Double.parseDouble(campo1.getText().toString())+
                            operacion.valoresUnidades().get(operacion.valoresUnidades().size()-2))/
                                    operacion.valoresUnidades().get(0);
                            break;
                        case 3:
                            unidad_1 = (Double.parseDouble(campo1.getText().toString())+
                                    operacion.valoresUnidades().get(operacion.valoresUnidades().size()-1))/
                                    operacion.valoresUnidades().get(1);
                            break;
                        default:
                            unidad_1 = Double.parseDouble(campo1.getText().toString());
                            break;
                    }
                }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        arrayUnidades2 = new String[] { operacion.getUnits()[0],
                operacion.getUnits()[4], operacion.getUnits()[5], operacion.getUnits()[6]};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_dropdown_item, arrayUnidades2);
        spUnidades2.setAdapter(adapter2);
        spUnidades2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
                        unidad_2 = Double.parseDouble(campo2.getText().toString());
                        break;
                    case 2:
                        unidad_2 = Double.parseDouble(campo2.getText().toString())/
                        operacion.valoresUnidades().get(2);
                        break;
                    case 3:
                        unidad_2 = Double.parseDouble(campo2.getText().toString())/
                        operacion.valoresUnidades().get(3);
                        break;
                    default:
                        unidad_2 = Double.parseDouble(campo2.getText().toString());
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        // Realizar cálculos
        Button botonCalculo = (Button) findViewById(R.id.calcBtn);
        botonCalculo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double resultado;
                valoresCampos.put(operacion.getInputs()[0], unidad_1);
                valoresCampos.put(operacion.getInputs()[1], unidad_2);
                valoresCampos.put(operacion.getInputs()[2],
                        Double.parseDouble(campo3.getText().toString()));
                resultado = operacion.calculateResult(valoresCampos);
                textoResultado.setText(String.format(Locale.ENGLISH,"%.4f", resultado));
            }
        });

    }
}
