package com.example.edwin.ecuacionEstado;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Dieterici extends AppCompatActivity {

    public static final String VARIABLE = "Variable";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dieterici);

        Intent intent = getIntent();
    }

    public void calcularPresion(View view) {
        Intent intPresion = new Intent(this, Calculo.class);
        intPresion.putExtra(VARIABLE, new PresionDieterici());
        startActivity(intPresion);
    }
}
