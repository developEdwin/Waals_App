package com.example.edwin.ecuacionEstado;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class EcuacionGasIdeal extends AppCompatActivity {

    public static final String VARIABLE = "Variable";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ecuaciones);

    }

    public void calcularTemperatura(View view) {
        Intent intTemp = new Intent(EcuacionGasIdeal.this, Calculo.class);
        intTemp.putExtra(VARIABLE, new Temperatura());
        startActivity(intTemp);
    }

    public void calcularPresion(View view) {
        Intent intPresion = new Intent(EcuacionGasIdeal.this, Calculo.class);
        intPresion.putExtra(VARIABLE, new Presion());
        startActivity(intPresion);
    }

    public void calcularVolumen(View view) {
        Intent intVolumen = new Intent(EcuacionGasIdeal.this, Calculo.class);
        intVolumen.putExtra(VARIABLE, new Volumen());
        startActivity(intVolumen);
    }

}
