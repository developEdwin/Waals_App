package com.example.edwin.ecuacionEstado;

import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

interface IdealGasInterface extends Parcelable {
    double consR = 0.0820574;
    String[] getInputs();
    double calculateResult(HashMap<String, Double> inputs);
    String getName();
    String[] getUnits();
    ArrayList<Double> valoresUnidades();
}