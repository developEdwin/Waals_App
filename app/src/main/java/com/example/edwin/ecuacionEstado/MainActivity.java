package com.example.edwin.ecuacionEstado;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends AppCompatActivity {

    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        spinner = (Spinner) findViewById(R.id.lista_ecuaciones);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.arr_lista_ecuaciones, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        break;
                    case 1:
//                        Toast.makeText(getApplicationContext(), "Ecuacion prueba",
//                                Toast.LENGTH_SHORT).show();
                        mandarEcuacionGasIdeal(view);
                        break;
                    case 2:
                        mandarDieterici(view);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void mandarEcuacionGasIdeal(View view) {
        Intent intent = new Intent(getApplicationContext(), EcuacionGasIdeal.class);
        startActivity(intent);
    }

    public void mandarDieterici(View view) {
        Intent intent = new Intent(this, Dieterici.class);
        startActivity(intent);
    }
}