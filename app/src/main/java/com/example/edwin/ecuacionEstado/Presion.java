package com.example.edwin.ecuacionEstado;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

class Presion implements IdealGasInterface {

    private HashMap<String, Double> map1;

    Presion() {
        map1 = new HashMap<>();
        map1.put("volumen", 1.0);
    }

    private Presion(Parcel in) {
        map1 = new HashMap<>();
        map1.put("moles", in.readDouble());
        map1.put("temperatura", in.readDouble());
        map1.put("volumen", in.readDouble());
    }

    @Override
    public String[] getInputs() {
        return new String[]{"temperatura", "volumen", "moles"};
    }

    @Override
    public String[] getUnits() {
        return new String[]{"Unidades", "K", "°F", "°C", "L", "m^3", "mL"};
    }

    @Override
    public double calculateResult(HashMap<String, Double> inputs) {
        double moles = inputs.get("moles");
        double temperatura = inputs.get("temperatura");
        double volumen = inputs.get("volumen");
        return (consR * moles * temperatura) / (volumen);
    }

    @Override
    public String getName() {
        return "Presión";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public ArrayList<Double> valoresUnidades() {
        return new ArrayList<Double>() {{
            add(1.8);
            add(1.0);
            add(0.001);
            add(1000.0);
            add(459.67);
            add(273.15);
        }};
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeMap(map1);
    }

    public static final Parcelable.Creator<Presion> CREATOR = new Parcelable.Creator<Presion>() {
        @Override
        public Presion createFromParcel(Parcel in) {
            return new Presion(in);
        }

        public Presion[] newArray(int size) {
            return new Presion[size];
        }
    };

}
