package com.example.edwin.ecuacionEstado;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.Math;

import java.util.ArrayList;
import java.util.HashMap;

class PresionDieterici implements IdealGasInterface {

    private HashMap<String, Double> map1;

    PresionDieterici() {
        map1 = new HashMap<>();
        map1.put("volumen", 1.0);
    }

    private PresionDieterici(Parcel in) {
        map1 = new HashMap<>();
        map1.put("moles", in.readDouble());
        map1.put("temperatura", in.readDouble());
        map1.put("volumen", in.readDouble());
    }

    @Override
    public String[] getInputs() {
        return new String[]{"temperatura", "volumen", "moles"};
    }

    @Override
    public String[] getUnits() {
        return new String[]{"Unidades", "K", "°F", "°C", "L", "m^3", "mL"};
    }

    @Override
    public double calculateResult(HashMap<String, Double> inputs) {
        double moles = inputs.get("moles");
        double temperatura = inputs.get("temperatura");
        double volumen = inputs.get("volumen");
        double aDieterici = 5.46;
        double bDieterici = 0.0305;
        return (consR*temperatura*Math.exp(-aDieterici/(consR*temperatura*volumen)))/(volumen - bDieterici);
    }

    @Override
    public ArrayList<Double> valoresUnidades() {
        return new ArrayList<Double>() {{
            add(1.0);
            add(1.0);
            add(1.0);
            add(1.0);
            add(1.0);
        }};
    }
    @Override
    public String getName() {
        return "Presión";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeMap(map1);
    }

    public static final Parcelable.Creator<PresionDieterici> CREATOR = new Parcelable.Creator<PresionDieterici>() {
        @Override
        public PresionDieterici createFromParcel(Parcel in) {
            return new PresionDieterici(in);
        }

        public PresionDieterici[] newArray(int size) {
            return new PresionDieterici[size];
        }
    };
}
