package com.example.edwin.ecuacionEstado;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

class Temperatura implements IdealGasInterface {

    private HashMap<String,Double> map;

    Temperatura() {
        map = new HashMap<>();
    }

    private Temperatura(Parcel in) {
        map = new HashMap<>();
        map.put("moles", in.readDouble());
        map.put("presion", in.readDouble());
        map.put("volumen", in.readDouble());
    }

    @Override
    public String[] getInputs() {
        return new String[]{"presion", "volumen", "moles"};
    }

    @Override
    public String[] getUnits() {
        return new String[]{"Unidades","atm", "psi", "mmHg", "L", "m^3", "mL"};
    }

    @Override
    public double calculateResult(HashMap<String, Double> inputs) {
        double moles = inputs.get("moles");
        double presion = inputs.get("presion");
        double volumen = inputs.get("volumen");
        return presion * volumen /(consR* moles);
    }

    @Override
    public String getName() {
        return "Temperatura";
    }

    @Override
    public ArrayList<Double> valoresUnidades() {
        return new ArrayList<Double>() {{
            add(14.696);
            add(760.0);
            add(0.001);
            add(1000.0);
            add(0.0);
            add(0.0);
        }};
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeMap(map);
    }

    public static final Parcelable.Creator<Temperatura> CREATOR = new Parcelable.Creator<Temperatura>() {
        @Override
        public Temperatura createFromParcel(Parcel in) {
            return new Temperatura(in);
        }

        public Temperatura[] newArray(int size) {
            return new Temperatura[size];
        }
    };
}
