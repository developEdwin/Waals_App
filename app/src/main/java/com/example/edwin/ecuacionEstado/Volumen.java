package com.example.edwin.ecuacionEstado;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;

class Volumen implements IdealGasInterface {
    
    private HashMap<String, Double> map2;

    Volumen() {
        map2 = new HashMap<>();
        map2.put("presion", 1.0);
    }

    private Volumen(Parcel in) {
        map2 = new HashMap<>();
        map2.put("moles", in.readDouble());
        map2.put("temperatura", in.readDouble());
        map2.put("presion", in.readDouble());
    }

    @Override
    public String[] getInputs() {
        return new String[]{"temperatura", "presion", "moles"};
    }

    @Override
    public String[] getUnits() {
        return new String[]{"Unidades", "K", "°F", "°C", "atm", "psi", "mmHg"};
    }

    @Override
    public double calculateResult(HashMap<String, Double> inputs) {
        double moles = inputs.get("moles");
        double temperatura = inputs.get("temperatura");
        double presion = inputs.get("presion");
        return (consR * moles * temperatura) / (presion);
    }

    @Override
    public ArrayList<Double> valoresUnidades() {
        return new ArrayList<Double>() {{
            add(1.8);
            add(1.0);
            add(14.696);
            add(760.0);
            add(459.67);
            add(273.15);
        }};
    }

    @Override
    public String getName() {
        return "Volumen";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeMap(map2);
    }

    public static final Parcelable.Creator<Volumen> CREATOR = new Parcelable.Creator<Volumen>() {
        @Override
        public Volumen createFromParcel(Parcel in) {
            return new Volumen(in);
        }

        public Volumen[] newArray(int size) {
            return new Volumen[size];
        }
    };
}
